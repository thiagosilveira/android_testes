package com.example.tsilveira.simplebutton;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "MainActivityFragment";

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main, container, false);

        Button btnTeste1 = (Button) v.findViewById(R.id.btnTeste1);
        Button btnTeste2 = (Button) v.findViewById(R.id.btnTeste2);

        /*btnTeste2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG,"Logando Clique do Teste 2");

            }
        });*/

        btnTeste1.setOnClickListener(this);
        btnTeste2.setOnClickListener(this);

        return v;

    }

    /*public void onBtnTeste1Clicked(View v) {

        Log.d(TAG,"Logando Clique do Teste 1");

    }*/

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnTeste1:
                Log.d(TAG,"Logando Clique do Teste 1");
                break;

            case R.id.btnTeste2:
                Log.d(TAG,"Logando Clique do Teste 2");
                break;

            default:
                Log.d(TAG,"NADA CLICADO");
                break;

        };

    }
}
